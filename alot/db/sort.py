# Copyright (C) 2012 Anton Khirnov <anton@khirnov.net>
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import notmuch2

ORDER = notmuch2.Database.SORT

NAME = {
    'oldest_first'  : ORDER.OLDEST_FIRST,
    'newest_first'  : ORDER.NEWEST_FIRST,
    'message_id'    : ORDER.MESSAGE_ID,
    'unsorted'      : ORDER.UNSORTED,
}
