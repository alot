# Copyright (C) 2011-2012  Patrick Totzke <patricktotzke@gmail.com>
# Copyright © 2018 Dylan Baker
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import os.path

def _humanize_size(size):
    """Create a nice human readable representation of the given number
    (understood as bytes) using the "KiB" and "MiB" suffixes to indicate
    kibibytes and mebibytes. A kibibyte is defined as 1024 bytes (as opposed to
    a kilobyte which is 1000 bytes) and a mibibyte is 1024**2 bytes (as opposed
    to a megabyte which is 1000**2 bytes).

    :param size: the number to convert
    :type size: int
    :returns: the human readable representation of size
    :rtype: str
    """
    for factor, format_string in ((1, '%iB'),
                                  (1024, '%iKiB'),
                                  (1024 * 1024, '%.1fMiB')):
        if size / factor < 1024:
            return format_string % (size / factor)
    return format_string % (size / factor)



class Attachment:
    """represents a mail attachment"""

    data         = None
    content_type = None
    filename     = None
    params       = None

    def __init__(self, data, ctype, filename, params):
        self.data         = data
        self.content_type = ctype
        self.params       = params

        if self.filename:
            # make sure the filename is a relative path
            # that does not go upwards
            filename = os.path.normpath(filename)
            if filename.startswith('/') or filename.startswith('..'):
                raise ValueError('Dangerous attachment filename: %s' % filename)

        self.filename = filename

    def __str__(self):
        ret = self.content_type
        if self.filename:
            ret += ':' + self.filename
        ret += ' (%s)' % _humanize_size(len(self.data))

        return ret

    @property
    def content_maintype(self):
        return self.content_type.partition('/')[0]
    @property
    def content_subtype(self):
        return self.content_type.partition('/')[2]
