# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

FROM                = 'From'
TO                  = 'To'
CC                  = 'Cc'
BCC                 = 'Bcc'
REPLY_TO            = 'Reply-To'
MAIL_REPLY_TO       = 'Mail-Reply-To'
MAIL_FOLLOWUP_TO    = 'Mail-Followup-To'

SUBJECT             = 'Subject'
DATE                = 'Date'
KEYWORDS            = 'Keywords'

MESSAGE_ID          = 'Message-ID'
USER_AGENT          = 'User-Agent'

MIME_VERSION        = 'MIME-Version'

IN_REPLY_TO         = 'In-Reply-To'
REFERENCES          = 'References'

LIST_ID             = 'List-ID'
X_BEEN_THERE        = 'X-BeenThere'
X_MAILING_LIST      = 'X-Mailing-List'
