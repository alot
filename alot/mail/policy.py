# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import email.policy         as _p
import email.headerregistry as _hdr

from . import headers as HDR

# here we define our custom policy that handles additional headers

_headers = {
    HDR.X_BEEN_THERE        : _hdr.UniqueAddressHeader,
    HDR.X_MAILING_LIST      : _hdr.UniqueAddressHeader,
    HDR.MAIL_FOLLOWUP_TO    : _hdr.UniqueAddressHeader,
    HDR.MAIL_REPLY_TO       : _hdr.UniqueAddressHeader,
}

# derive from SMTP, but create a new instance to get our own
# instance of header_factory
p = _p.EmailPolicy() + _p.SMTP

for h, t in _headers.items():
    p.header_factory.map_to_type(h, t)
