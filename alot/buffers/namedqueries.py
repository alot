# Copyright (C) 2011-2018  Patrick Totzke <patricktotzke@gmail.com>
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import urwid

from .buffer                import Buffer
from ..settings.const       import settings
from ..widgets.namedqueries import QuerylineWidget


class NamedQueriesBuffer(Buffer):
    """lists named queries present in the notmuch database"""

    modename = 'namedqueries'

    _dbman   = None
    _queries = None

    def __init__(self, dbman, filtfun):
        self._dbman = dbman

        self.filtfun = filtfun
        self.isinitialized = False
        self.querylist = None
        self.rebuild()

        super().__init__()

    def rebuild(self):
        queries = self._dbman.get_named_queries()

        sort_hook = settings.get_hook('namedqueries_sort')
        if sort_hook:
            queries = sort_hook(queries)
        else:
            queries = list(queries.items())

        self._queries = queries

        if self.isinitialized:
            focusposition = self.querylist.get_focus()[1]
        else:
            focusposition = 0

        lines = []
        for num, (key, value) in enumerate(queries):
            count = self._dbman.count_messages('query:"%s"' % key)
            count_unread = self._dbman.count_messages('query:"%s" and '
                                                        'tag:unread' % key)
            line = QuerylineWidget(key, value, count, count_unread)

            if (num % 2) == 0:
                attr = settings.get_theming_attribute('namedqueries',
                                                      'line_even')
            else:
                attr = settings.get_theming_attribute('namedqueries',
                                                      'line_odd')
            focus_att = settings.get_theming_attribute('namedqueries',
                                                       'line_focus')

            line = urwid.AttrMap(line, attr, focus_att)
            lines.append(line)

        self.querylist = urwid.ListBox(urwid.SimpleListWalker(lines))
        self.body = self.querylist

        self.querylist.set_focus(focusposition % len(queries))

        self.isinitialized = True

    def focus_first(self):
        """Focus the first line in the query list."""
        self.body.set_focus(0)

    def focus_last(self):
        allpos = self.querylist.body.positions(reverse=True)
        if allpos:
            lastpos = allpos[0]
            self.body.set_focus(lastpos)

    def get_selected_query(self):
        """returns selected query"""
        return self.querylist.get_focus()[0].original_widget.query

    async def get_info(self):
        info = {}

        info['query_count'] = len(self._queries)

        return info
