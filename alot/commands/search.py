# Copyright (C) 2011-2012  Patrick Totzke <patricktotzke@gmail.com>
# Copyright © 2018 Dylan Baker
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import argparse
import logging

from .          import Command, registerCommand
from .globals   import PromptCommand
from .globals   import MoveCommand
from .common    import RetagPromptCommand

from ..             import buffers
from ..             import commands
from ..db.errors    import DatabaseROError
from ..db.sort      import NAME as SORT_NAME


MODE = 'search'


@registerCommand(MODE, 'select')
class OpenThreadCommand(Command):

    """open thread in a new buffer"""

    def apply(self, ui):
        thread = ui.current_buffer.get_selected_thread()
        if thread:
            query = ui.current_buffer.querystring
            logging.info('open thread view for %s', thread)

            tb = buffers.ThreadBuffer(ui, thread)
            ui.buffer_open(tb)
            tb.focus_next_matching(query)

_opt_reverse = (['--reverse'],
    { 'help'    : 'display messages in reverse order',
      'default' : False,
      'action'  : 'store_true' }
)

@registerCommand(MODE, 'refine', help='refine query', arguments=[
    (['--sort'], {'help': 'sort order',
        'choices': list(SORT_NAME.keys())}), _opt_reverse,
    (['query'], {'nargs': argparse.REMAINDER, 'help': 'search string'})])
@registerCommand(MODE, 'sort', help='set sort order', arguments=[
    _opt_reverse,
    (['sort'], {'help': 'sort order', 'choices': [
        'oldest_first', 'newest_first', 'message_id', 'unsorted']}),
])
@registerCommand(MODE, 'reverse',
    help   = 'toggle whether the sorting order is reversed',
    forced = { 'toggle_reverse' : True })
class RefineCommand(Command):

    _reverse        = None
    _toggle_reverse = None

    """refine the querystring of this buffer"""
    def __init__(self, query=None, sort=None, reverse = None,
                 toggle_reverse = False, **kwargs):
        """
        :param query: new querystring given as list of strings as returned by
                      argparse
        :type query: list of str
        """
        if query is None:
            self.querystring = None
        else:
            self.querystring = ' '.join(query)
        self.sort_order      = SORT_NAME[sort] if sort else None
        self._reverse        = reverse
        self._toggle_reverse = toggle_reverse
        super().__init__(**kwargs)

    def apply(self, ui):
        if self.querystring or self.sort_order or self._toggle_reverse:
            sbuffer = ui.current_buffer
            oldquery = sbuffer.querystring
            if self.querystring not in [None, oldquery]:
                sbuffer.querystring = self.querystring
                sbuffer = ui.current_buffer
            if self.sort_order:
                sbuffer.sort_order = self.sort_order
            if self._reverse is not None:
                sbuffer.reverse = self._reverse
            if self._toggle_reverse:
                sbuffer.reverse = not sbuffer.reverse

            sbuffer.rebuild()
            ui.update()
        else:
            ui.notify('empty query string')


@registerCommand(MODE, 'refineprompt')
class RefinePromptCommand(Command):

    """prompt to change this buffers querystring"""
    repeatable = True

    async def apply(self, ui):
        sbuffer = ui.current_buffer
        oldquery = sbuffer.querystring
        return await ui.apply_command(PromptCommand('refine ' + oldquery))


RetagPromptCommand = registerCommand(MODE, 'retagprompt')(RetagPromptCommand)

_tag_help_common = 'By default, this command applies only to the currently '    \
    'focused thread, and furthermore only to those messages in the thread '     \
    'that match the search query. This behaviour may be changed by the --all '  \
    'and --thread options.'

_tag_opt_all = (['--all'],
    {'action'   : 'store_true',
     'dest'     : 'all_threads',
     'default'  : False,
     'help'     : 'apply to all threads rather than only the focused one'})
_tag_opt_thread = (['--thread'],
    {'action'   : 'store_true',
     'dest'     : 'thread',
     'default'  : False,
     'help'     : 'apply to all messages in the thread rather than only those'
                  'matching the query'})
_tag_opt_tags = (['tags'], {'help': 'comma separated list of tags'})

@registerCommand(
    MODE, 'tag', forced={'action': 'add'},
    arguments = [ _tag_opt_all, _tag_opt_thread, _tag_opt_tags ],
    help = 'Add tags to messages. ' + _tag_help_common,
)
@registerCommand(
    MODE, 'retag', forced={'action': 'set'},
    arguments = [ _tag_opt_all, _tag_opt_thread, _tag_opt_tags ],
    help = 'Set tags to messages. ' + _tag_help_common,
)
@registerCommand(
    MODE, 'untag', forced={'action': 'remove'},
    arguments = [ _tag_opt_all, _tag_opt_thread, _tag_opt_tags ],
    help = 'Remove tags from messages. ' + _tag_help_common,
)
@registerCommand(
    MODE, 'toggletags', forced={'action': 'toggle'},
    arguments = [ _tag_opt_tags ],
    help='flip presence of tags on the selected thread: a tag is considered present '
         'and will be removed if at least one message in this thread is '
         'tagged with it')
class TagCommand(Command):

    """manipulate message tags"""
    repeatable = True

    _tags        = None
    _action      = None
    _all_threads = None
    _thread      = None

    def __init__(self, tags = '', action = 'add', all_threads = False,
                 thread = False, **kwargs):
        """
        :param tags: comma separated list of tagstrings to set
        :type tags: str
        :param action: adds tags if 'add', removes them if 'remove', adds tags
                       and removes all other if 'set' or toggle individually if
                       'toggle'
        :type action: str
        """
        self._tags        = frozenset(filter(None, tags.split(',')))
        self._action      = action
        self._all_threads = all_threads
        self._thread      = thread

        super().__init__(**kwargs)

    async def apply(self, ui):
        searchbuffer = ui.current_buffer
        threadline_widget = searchbuffer.get_selected_threadline()
        # pass if the current buffer has no selected threadline
        # (displays an empty search result)
        if threadline_widget is None:
            return

        if self._all_threads:
            if self._thread:
                ui.notify('--all with --thread is not supported yet',
                          priority = 'error')
                return
            testquery = searchbuffer.querystring
        else:
            thread    = threadline_widget.get_thread()
            testquery = 'thread:' + thread.id

            if not self._thread:
                testquery += ' and (%s)' % searchbuffer.querystring

        if self._action == 'add':
            task = ui.dbman.tags_add(testquery, self._tags)
        if self._action == 'set':
            task = ui.dbman.tags_set(testquery, self._tags)
        elif self._action == 'remove':
            task =  ui.dbman.tags_remove(testquery, self._tags)
        elif self._action == 'toggle':
            if not self._all_threads:
                to_remove = set()
                to_add = set()
                for t in self._tags:
                    if t in thread.get_tags():
                        to_remove.add(t)
                    else:
                        to_add.add(t)

                write = ui.dbman.db_write_create()
                write.queue_tag_remove(to_remove, 'thread:' + thread.id)
                write.queue_tag_add(to_add,       'thread:' + thread.id)
                task = write.apply()

        try:
            await task
        except DatabaseROError:
            ui.notify('index in read-only mode', priority='error')
            return

        # update total result count
        if not self._all_threads:
            threadline_widget.rebuild()
            searchbuffer.result_count = searchbuffer.dbman.count_messages(
                searchbuffer.querystring)
        else:
            searchbuffer.rebuild()

        ui.update()

@registerCommand(
    MODE, 'move', help='move focus in search buffer',
    arguments=[(['movement'], {'nargs': argparse.REMAINDER, 'help': 'last'})])
class MoveFocusCommand(MoveCommand):

    def apply(self, ui):
        logging.debug(self.movement)
        if self.movement == 'last':
            ui.current_buffer.focus_last()
            ui.update()
        else:
            MoveCommand.apply(self, ui)
