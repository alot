# Copyright (C) 2011-2012  Patrick Totzke <patricktotzke@gmail.com>
# Copyright © 2018 Dylan Baker
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

from .                  import Command
from .globals           import PromptCommand
from ..settings.const   import settings

class RetagPromptCommand(Command):

    """prompt to retag selected thread's or message's tags"""
    async def apply(self, ui):
        get_selected_item = getattr(ui.current_buffer, {
                'search': 'get_selected_thread',
                'thread': 'get_selected_message'}[ui.mode])
        item = get_selected_item()
        if not item:
            return

        exclude_tags = frozenset(settings.get('property_tags'))
        tags = map(lambda x: ("%s" % x) if ' ' in x else x,
                   item.get_tags() - exclude_tags)

        initial_tagstring = ','.join(sorted(tags))
        if initial_tagstring:
            initial_tagstring += ','

        r = await ui.apply_command(PromptCommand('retag ' + initial_tagstring))
        return r
