# Copyright (C) 2011-2012  Patrick Totzke <patricktotzke@gmail.com>
# Copyright © 2018 Dylan Baker
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import argparse
import asyncio
import email
import email.policy
import logging
import mailcap
import os
import shutil
import subprocess
import tempfile

import urwid
from   urwid.util import detected_encoding

from .          import Command, registerCommand
from .common    import RetagPromptCommand
from .envelope  import SendCommand
from .globals   import ComposeCommand
from .globals   import MoveCommand
from .globals   import CommandCanceled

from ..completion.contacts  import ContactsCompleter
from ..completion.path      import PathCompleter
from ..db.errors            import DatabaseROError
from ..helper               import formataddr
from ..helper               import split_commandstring
from ..mail.attachment      import Attachment
from ..mail.envelope        import Envelope
from ..mail                 import headers as HDR
from ..mail                 import reply
from ..settings.const       import settings
from ..utils                import argparse as cargparse
from ..utils.mailcap        import MailcapHandler

MODE = 'thread'

@registerCommand(MODE, 'reply', arguments=[
    (['--all'], {'action': 'store_true', 'help': 'reply to all'}),
    (['--list'], {'action': cargparse.BooleanAction, 'default': None,
                  'dest': 'listreply', 'help': 'reply to list'}),
    (['--spawn'], {'action': cargparse.BooleanAction, 'default': None,
                   'help': 'open editor in new window'})])
class ReplyCommand(Command):

    """reply to message"""
    repeatable = True

    def __init__(self, all=False, listreply=None, spawn=None,
                 **kwargs):
        """
        :param all: group reply; copies recipients from Bcc/Cc/To to the reply
        :type all: bool
        :param listreply: reply to list; autodetect if unset and enabled in
                          config
        :type listreply: bool
        :param spawn: force spawning of editor in a new terminal
        :type spawn: bool
        """
        self.groupreply = all
        self._force_list_reply = listreply
        self.force_spawn = spawn
        super().__init__(**kwargs)

    def _list_reply(self, message):
        # Auto-detect ML
        if HDR.LIST_ID in message.headers and self._force_list_reply is None:
            return settings.get('auto_replyto_mailinglist')

        return bool(self._force_list_reply)

    async def apply(self, ui):
        message = ui.current_buffer.get_selected_message()

        # construct reply headers
        headers = {}

        headers[HDR.SUBJECT] = reply.subject(message)

        # set From-header and sending account
        try:
            from_header, account = reply.determine_account(message.headers,
                                                           reply.ACT_REPLY)
        except AssertionError as e:
            ui.notify(str(e), priority='error')
            return
        headers[HDR.FROM] = from_header

        # set the recipients
        mode = reply.ReplyMode.LIST  if self._list_reply(message) else   \
               reply.ReplyMode.GROUP if self.groupreply           else   \
               reply.ReplyMode.AUTHOR

        to, cc = reply.determine_recipients(message, account, mode)
        # TODO keep those as lists?
        headers[HDR.TO] = str(to)
        if cc:
            headers[HDR.CC] = str(cc)

        # set In-Reply-To + References headers
        headers[HDR.IN_REPLY_TO] = '<%s>' % message.id
        headers[HDR.REFERENCES]  = reply.references(message)

        # if any of the recipients is a mailinglist that we are subscribed to,
        # set Mail-Followup-To header so that duplicates are avoided
        # to and cc are already cleared of our own address
        mft = reply.mail_followup_to(to + cc)
        if mft:
            headers[HDR.MAIL_FOLLOWUP_TO] = str(mft)

        body_text = reply.body_text(message, ui)

        # construct the envelope
        envelope = Envelope(headers = headers, bodytext = body_text,
                            account = account, replied = message)

        # continue to compose
        await ui.apply_command(ComposeCommand(envelope=envelope,
                                              spawn=self.force_spawn,
                                              encrypt = message.body.is_encrypted))


@registerCommand(MODE, 'forward', arguments=[
    (['--attach'], {'action': 'store_true', 'help': 'attach original mail'}),
    (['--spawn'], {'action': cargparse.BooleanAction, 'default': None,
                   'help': 'open editor in new window'})])
class ForwardCommand(Command):

    """forward message"""
    repeatable = True

    def __init__(self, attach=True, spawn=None, **kwargs):
        """
        :param attach: attach original mail instead of inline quoting its body
        :type attach: bool
        :param spawn: force spawning of editor in a new terminal
        :type spawn: bool
        """
        self.inline = not attach
        self.force_spawn = spawn
        super().__init__(**kwargs)

    async def apply(self, ui):
        message = ui.current_buffer.get_selected_message()

        envelope = Envelope(passed = message)

        if self.inline:  # inline mode
            # set body text
            name, address = message.get_author()
            timestamp     = message.date

            qf = settings.get_hook('forward_prefix')
            if qf:
                mail  = email.message_from_bytes(message.as_bytes(),
                                                 policy = email.policy.SMTP)
                quote = qf(name, address, timestamp,
                           message=mail, ui=ui, dbm=ui.dbman)
            else:
                quote = 'Forwarded message from %s (%s):\n' % (
                    name or address, timestamp)
            mailcontent = quote
            quotehook = settings.get_hook('text_quote')
            if quotehook:
                mailcontent += quotehook(message.get_body_text())
            else:
                quote_prefix = settings.get('quote_prefix')
                for line in message.get_body_text().splitlines():
                    mailcontent += quote_prefix + line + '\n'

            envelope.body = mailcontent

            for a in message.iter_attachments():
                envelope.attach(a)

        else:  # attach original mode
            a = Attachment(message.as_bytes(), 'message/rfc822', None, ())
            envelope.attach(a)

        # copy subject
        subject = message.headers.get(HDR.SUBJECT) or ''
        subject = 'Fwd: ' + subject
        forward_subject_hook = settings.get_hook('forward_subject')
        if forward_subject_hook:
            subject = forward_subject_hook(subject)
        else:
            fsp = settings.get('forward_subject_prefix')
            if not subject.startswith(('Fwd:', fsp)):
                subject = fsp + subject
        envelope.add(HDR.SUBJECT, subject)

        # set From-header and sending account
        try:
            from_header, account = reply.determine_account(message.headers,
                                                           reply.ACT_FWD)
        except AssertionError as e:
            ui.notify(str(e), priority='error')
            return
        envelope.add(HDR.FROM, from_header)
        envelope.account = account

        # continue to compose
        await ui.apply_command(ComposeCommand(envelope=envelope,
                                              spawn=self.force_spawn))


@registerCommand(MODE, 'bounce')
class BounceMailCommand(Command):

    """directly re-send selected message"""
    repeatable = True

    async def apply(self, ui):
        message = ui.current_buffer.get_selected_message()
        mail    = email.message_from_bytes(message.as_bytes(),
                                           policy = email.policy.SMTP)

        # look if this makes sense: do we have any accounts set up?
        my_accounts = settings.get_accounts()
        if not my_accounts:
            ui.notify('no accounts set', priority='error')
            return

        # remove "Resent-*" headers if already present
        del mail['Resent-From']
        del mail['Resent-To']
        del mail['Resent-Cc']
        del mail['Resent-Date']
        del mail['Resent-Message-ID']

        # set Resent-From-header and sending account
        try:
            resent_from_header, account = reply.determine_account(message.headers,
                                                                  reply.ACT_BOUNCE)
        except AssertionError as e:
            ui.notify(str(e), priority='error')
            return
        mail['Resent-From'] = resent_from_header

        # set Reset-To
        allbooks = not settings.get('complete_matching_abook_only')
        logging.debug('allbooks: %s', allbooks)
        if account is not None:
            abooks = settings.get_addressbooks(order=[account],
                                               append_remaining=allbooks)
            logging.debug(abooks)
            completer = ContactsCompleter(abooks)
        else:
            completer = None
        to = await ui.prompt('To', completer=completer,
                             history=ui.recipienthistory)
        if to is None:
            raise CommandCanceled()

        mail['Resent-To'] = to.strip(' \t\n,')

        logging.debug("bouncing mail")
        logging.debug(mail.__class__)

        await ui.apply_command(SendCommand(mail=mail))


@registerCommand(MODE, 'editnew', arguments=[
    (['--spawn'], {'action': cargparse.BooleanAction, 'default': None,
                   'help': 'open editor in new window'})])
class EditNewCommand(Command):

    """edit message in as new"""
    def __init__(self, message=None, spawn=None, **kwargs):
        """
        :param message: message to reply to (defaults to selected message)
        :type message: `alot.db.message.Message`
        :param spawn: force spawning of editor in a new terminal
        :type spawn: bool
        """
        self.message = message
        self.force_spawn = spawn
        super().__init__(**kwargs)

    async def apply(self, ui):
        if not self.message:
            self.message = ui.current_buffer.get_selected_message()
        # copy most tags to the envelope
        tags = set(self.message.get_tags())
        tags.difference_update({'inbox', 'sent', 'draft', 'killed', 'replied',
                                'signed', 'encrypted', 'unread', 'attachment'})
        # set body text
        mailcontent = self.message.get_body_text()
        envelope = Envelope(bodytext=mailcontent, tags=tags)

        # copy selected headers
        to_copy = [HDR.SUBJECT, HDR.FROM, HDR.TO, HDR.CC, HDR.BCC,
                   HDR.IN_REPLY_TO, HDR.REFERENCES]
        for key in to_copy:
            if key in self.message.headers:
                value = self.message.headers[key][0]
                envelope.add(key, value)

        # copy attachments
        for b in self.message.iter_attachments():
            envelope.attach(b)

        await ui.apply_command(ComposeCommand(envelope=envelope,
                                              spawn=self.force_spawn,
                                              omit_signature=True))


@registerCommand(
    MODE, 'togglesource', help='display message source',
    forced={'raw': 'toggle'},
    arguments=[(['query'], {'help': 'query used to filter messages to affect',
                            'nargs': '*'})])
@registerCommand(
    MODE, 'toggleheaders', help='display all headers',
    forced={'all_headers': 'toggle'},
    arguments=[(['query'], {'help': 'query used to filter messages to affect',
                            'nargs': '*'})])
@registerCommand(
    MODE, 'indent', help='change message/reply indentation',
    arguments=[(['indent'], {'action': cargparse.ValidatedStoreAction,
                             'validator': cargparse.is_int_or_pm})])
@registerCommand(
    MODE, 'fold', help='change message folding level',
    arguments=[(['fold'], {'action': cargparse.ValidatedStoreAction,
                             'validator': cargparse.is_int_or_pm})])
@registerCommand(
    MODE, 'cycle_alt', help='cycle among MIME alternative parts',
    forced = {'cycle_alt' : True },)
@registerCommand(
    MODE, 'weight', help='change weight of the thread tree',
    arguments=[(['weight'], {'action': cargparse.ValidatedStoreAction,
                             'validator': cargparse.is_int_or_pm})])
class ChangeDisplaymodeCommand(Command):

    repeatable = True

    def __init__(self, query=None, raw=None, all_headers=None,
                 indent=None, fold = None, weight = None, cycle_alt = None,
                 **kwargs):
        """
        :param query: notmuch query string used to filter messages to affect
        :type query: str
        :param raw: display raw message text.
        :type raw: True, False, 'toggle' or None
        :param all_headers: show all headers (only visible if not in raw mode)
        :type all_headers: True, False, 'toggle' or None
        :param indent: message/reply indentation
        :type indent: '+', '-', or int
        :param fold: message fold level
        :type indent: '+', '-', or int
        """
        self.query = None
        if query:
            self.query = ' '.join(query)
        self.raw = raw
        self.all_headers = all_headers
        self.indent = indent
        self.fold   = fold
        self.weight = weight
        self.cycle_alt = cycle_alt
        super().__init__(**kwargs)

    def apply(self, ui):
        tbuffer = ui.current_buffer

        # set message/reply indentation if changed
        if self.indent is not None:
            if self.indent == '+':
                newindent = tbuffer._indent_width + 1
            elif self.indent == '-':
                newindent = tbuffer._indent_width - 1
            else:
                # argparse validation guarantees that self.indent
                # can be cast to an integer
                newindent = int(self.indent)
            # make sure indent remains non-negative
            tbuffer._indent_width = max(newindent, 0)
            tbuffer.rebuild()
            ui.update()

        if self.weight is not None:
            new_weight = tbuffer.msgtree_weight
            if self.weight == '+':
                new_weight += 0.1
            elif self.weight == '-':
                new_weight -= 0.1
            else:
                new_weight = self.weight

            tbuffer.msgtree_weight = new_weight

        logging.debug('matching lines %s...', self.query)
        if self.query is None:
            msg_wgts = [tbuffer.get_selected_message_widget()]
        else:
            msg_wgts = list(tbuffer.message_widgets())
            if self.query != '*':

                def matches(msgt):
                    msg = msgt.get_message()
                    return msg.matches(self.query)

                msg_wgts = [m for m in msg_wgts if matches(m)]

        for m in msg_wgts:
            # determine new display values for this message
            if self.raw == 'toggle':
                tbuffer.focus_selected_message()
            raw = not m.display_source if self.raw == 'toggle' else self.raw
            all_headers = not m.display_all_headers \
                if self.all_headers == 'toggle' else self.all_headers

            tbuffer.focus_selected_message()
            # set new values in messagetree obj
            if raw is not None:
                m.display_source = raw
            if all_headers is not None:
                m.display_all_headers = all_headers

            if self.fold is not None:
                if self.fold == '+':
                    m.foldlevel += 1
                elif self.fold == '-':
                    m.foldlevel -= 1
                else:
                    m.foldlevel = int(self.fold)

            if self.cycle_alt:
                m.cycle_alt()


@registerCommand(MODE, 'pipeto', arguments=[
    (['cmd'], {'help': 'shellcommand to pipe to', 'nargs': '+'}),
    (['--all'], {'action': 'store_true', 'help': 'pass all messages'}),
    (['--format'], {'help': 'output format', 'default': 'raw',
                    'choices': ['raw', 'decoded', 'id', 'filepath']}),
    (['--separately'], {'action': 'store_true',
                        'help': 'call command once for each message'}),
    (['--background'], {'action': 'store_true',
                        'help': 'don\'t stop the interface'}),
    (['--add_tags'], {'action': 'store_true',
                      'help': 'add \'Tags\' header to the message'}),
    (['--shell'], {'action': 'store_true',
                   'help': 'let the shell interpret the command'}),
    (['--notify_stdout'], {'action': 'store_true',
                           'help': 'display cmd\'s stdout as notification'}),
])
class PipeCommand(Command):

    """pipe message(s) to stdin of a shellcommand"""
    repeatable = True

    def __init__(self, cmd, all=False, separately=False, background=False,
                 shell=False, notify_stdout=False, format='raw',
                 add_tags=False, noop_msg='no command specified',
                 confirm_msg='', done_msg=None, **kwargs):
        """
        :param cmd: shellcommand to open
        :type cmd: str or list of str
        :param all: pipe all, not only selected message
        :type all: bool
        :param separately: call command once per message
        :type separately: bool
        :param background: do not suspend the interface
        :type background: bool
        :param shell: let the shell interpret the command
        :type shell: bool
        :param notify_stdout: display command\'s stdout as notification message
        :type notify_stdout: bool
        :param format: what to pipe to the processes stdin. one of:
            'raw': message content as is,
            'decoded': message content, decoded quoted printable,
            'id': message ids, separated by newlines,
            'filepath': paths to message files on disk
        :type format: str
        :param add_tags: add 'Tags' header to the message
        :type add_tags: bool
        :param noop_msg: error notification to show if `cmd` is empty
        :type noop_msg: str
        :param confirm_msg: confirmation question to ask (continues directly if
                            unset)
        :type confirm_msg: str
        :param done_msg: notification message to show upon success
        :type done_msg: str
        """
        super().__init__(**kwargs)
        if isinstance(cmd, str):
            cmd = split_commandstring(cmd)
        self.cmd = cmd
        self.whole_thread = all
        self.separately = separately
        self.background = background
        self.shell = shell
        self.notify_stdout = notify_stdout
        self.output_format = format
        self.add_tags = add_tags
        self.noop_msg = noop_msg
        self.confirm_msg = confirm_msg
        self.done_msg = done_msg

    async def apply(self, ui):
        # abort if command unset
        if not self.cmd:
            ui.notify(self.noop_msg, priority='error')
            return

        # get messages to pipe
        if self.whole_thread:
            thread = ui.current_buffer.thread
            if not thread:
                return
            to_print = thread.messages.values()
        else:
            to_print = [ui.current_buffer.get_selected_message()]

        # ask for confirmation if needed
        if self.confirm_msg:
            if (await ui.choice(self.confirm_msg, select='yes',
                                cancel='no')) == 'no':
                return

        # prepare message sources
        pipe_data = []
        logging.debug('PIPETO format')
        logging.debug(self.output_format)

        if self.output_format == 'id':
            pipe_data = [e.id.encode('utf-8') for e in to_print]
            separator = b'\n'
        elif self.output_format == 'filepath':
            pipe_data = [e.filename for e in to_print]
            separator = b'\n'
        else:
            separator = b'\n\n'
            for msg in to_print:
                if self.output_format == 'raw':
                    msg_data = msg.as_bytes()
                elif self.output_format == 'decoded':
                    # XXX HACK: filter out content-transfer-encoding, since we are writing out
                    # the decoder version
                    # should be properly handled elsewhere (where? how?)
                    headers = filter(lambda i: i[0].lower() != 'content-transfer-encoding', msg.headers.items())
                    headertext = '\n'.join([key + ': ' + val for key, val in headers])
                    bodytext = msg.get_body_text()
                    msgtext = '%s\n\n%s' % (headertext, bodytext)

                    msg_data = msgtext.encode('utf-8')

                if self.add_tags:
                    tags = 'Tags: ' + ','.join(msg.get_tags()) + '\n'
                    msg_data = tags.encode('utf-8') + msg_data

                pipe_data.append(msg_data)

        if not self.separately:
            pipe_data = [separator.join(pipe_data)]
        if self.shell:
            self.cmd = [' '.join(self.cmd)]

        # do the monkey
        for d in pipe_data:
            if self.background:
                logging.debug('call in background: %s', self.cmd)
                proc = subprocess.Popen(self.cmd,
                                        shell=True, stdin=subprocess.PIPE,
                                        stdout=subprocess.PIPE,
                                        stderr=subprocess.PIPE)
                out, err = proc.communicate(d)
                if self.notify_stdout:
                    ui.notify(out, block = True)
            else:
                with ui.paused():
                    logging.debug('call: %s', self.cmd)
                    # if proc.stdout is defined later calls to communicate
                    # seem to be non-blocking!
                    proc = subprocess.Popen(self.cmd, shell=True,
                                            stdin=subprocess.PIPE,
                                            # stdout=subprocess.PIPE,
                                            stderr=subprocess.PIPE)
                    out, err = proc.communicate(d)
            if err:
                ui.notify(err, priority='error')
                return

        # display 'done' message
        if self.done_msg:
            ui.notify(self.done_msg)


@registerCommand(MODE, 'print', arguments=[
    (['--all'], {'action': 'store_true', 'help': 'print all messages'}),
    (['--raw'], {'action': 'store_true', 'help': 'pass raw mail string'}),
    (['--separately'], {'action': 'store_true',
                        'help': 'call print command once for each message'}),
    (['--add_tags'], {'action': 'store_true',
                      'help': 'add \'Tags\' header to the message'}),
])
class PrintCommand(PipeCommand):

    """print message(s)"""
    repeatable = True

    def __init__(self, all=False, separately=False, raw=False, add_tags=False,
                 **kwargs):
        """
        :param all: print all, not only selected messages
        :type all: bool
        :param separately: call print command once per message
        :type separately: bool
        :param raw: pipe raw message string to print command
        :type raw: bool
        :param add_tags: add 'Tags' header to the message
        :type add_tags: bool
        """
        # get print command
        cmd = settings.get('print_cmd') or ''

        # set up notification strings
        if all:
            confirm_msg = 'print all messages in thread?'
            ok_msg = 'printed thread using %s' % cmd
        else:
            confirm_msg = 'print selected message?'
            ok_msg = 'printed message using %s' % cmd

        # no print cmd set
        noop_msg = 'no print command specified. Set "print_cmd" in the '\
            'global section.'

        super().__init__([cmd], all=all, separately=separately,
                         background=True,
                         shell=False,
                         format='raw' if raw else 'decoded',
                         add_tags=add_tags,
                         noop_msg=noop_msg, confirm_msg=confirm_msg,
                         done_msg=ok_msg, **kwargs)


@registerCommand(MODE, 'save', arguments=[
    (['--all'], {'action': 'store_true', 'help': 'save all attachments'}),
    (['path'], {'nargs': '?', 'help': 'path to save to'})])
class SaveAttachmentCommand(Command):

    """save attachment(s)"""
    def __init__(self, all=False, path=None, **kwargs):
        """
        :param all: save all, not only selected attachment
        :type all: bool
        :param path: path to write to. if `all` is set, this must be a
                     directory.
        :type path: str
        """
        super().__init__(**kwargs)
        self.all = all
        self.path = path

    def _save_attachment(self, ui, path, attachment):
        is_dir   = os.path.isdir(path)
        dst_path = path
        dst      = None

        try:
            if is_dir and not attachment.filename:
                # generate a random filename if we don't have one
                dst  = tempfile.NamedTemporaryFile(delete = False,
                                                   dir = dst_path)
                dst_path = f.name
            else:
                if is_dir:
                    dst_path = os.path.join(dst_path, attachment.filename)

                dst = open(dst_path, 'xb')

            dst.write(attachment.data)
            dst.close()
        except Exception as e:
            if dst:
                os.remove(dst_path)
                dst.close()
            if isinstance(e, IOError) or isinstance(e, OSError):
                ui.notify('Error saving attachment: %s' % str(e),
                          priority = 'error')
            else:
                raise
        else:
            ui.notify('saved %s as: %s' % (attachment, dst_path))

    async def apply(self, ui):
        pcomplete = PathCompleter()
        savedir   = settings.get('attachment_prefix', '~')
        path      = self.path

        if self.all:
            msg = ui.current_buffer.get_selected_message()
            if not path:
                path = await ui.prompt('save attachments to',
                                       text = os.path.join(savedir, ''),
                                       completer = pcomplete)
                if not path:
                    raise CommandCanceled()

            path = os.path.expanduser(path)
            if not os.path.isdir(path):
                ui.notify('not a directory: %s' % path,
                          priority = 'error')
                return

            for a in msg.iter_attachments():
                self._save_attachment(ui, path, a)
        else:  # save focussed attachment
            a = ui.current_buffer.get_selected_attachment()
            if not a:
                return

            if not path:
                msg = 'save attachment (%s) to ' % a.filename
                initialtext = os.path.join(savedir, '')
                path = await ui.prompt(msg, completer = pcomplete,
                                       text = savedir)
                if not path:
                    raise CommandCanceled()

            self._save_attachment(ui, path, a)

@registerCommand(MODE, 'openattachment')
class OpenAttachmentCommand(Command):

    """displays an attachment according to mailcap"""
    async def apply(self, ui):
        attachment = ui.current_buffer.get_selected_attachment()
        if attachment is None:
            return

        logging.info('open attachment: %s', attachment)

        data     = attachment.data
        mimetype = attachment.content_type
        params   = attachment.params
        fname    = attachment.filename

        h = MailcapHandler(data, mimetype, params, fname, 'view')
        if not h:
            ui.notify('No handler for: %s' % mimetype)
            return

        # TODO: hook for processing the command
        pager = None
        if h.copious_output:
            # pipe the command's output to a pager
            # TODO: allow configuring the pager from the config?
            pager = os.getenv('PAGER', 'less')
            if not shutil.which(pager):
                pager = None

        if h.needs_terminal or pager:
            with ui.paused(), h:
                cmd = ('( %s ) | %s' % (h.cmd, pager)) if pager else h.cmd

                logging.debug('Displaying part %s on terminal: %s',
                              mimetype, cmd)

                try:
                    result = subprocess.run(cmd, shell = True, check = True,
                                            input = h.stdin, stderr = subprocess.PIPE)
                except subprocess.CalledProcessError as e:
                    logging.error('Calling mailcap handler "%s" failed with code %d: %s',
                                  h.cmd, e.returncode,
                                  e.stderr.decode(detected_encoding, errors = 'backslashreplace'))
            return

        # does not need terminal - launch asynchronously
        async def view_attachment_task(h):
            with h:
                logging.debug('Displaying part %s asynchronously: %s',
                              mimetype, h.cmd)

                stdin  = subprocess.PIPE if h.stdin else subprocess.DEVNULL
                stdout = subprocess.DEVNULL
                stderr = subprocess.DEVNULL
                child = await asyncio.create_subprocess_shell(h.cmd, stdin, stdout, stderr)
                await child.communicate(h.stdin)

                if child.returncode != 0:
                    logging.error('Calling mailcap handler "%s" failed with code %d:',
                                  h.cmd, e.returncode)

        ui.run_task(view_attachment_task(h))

@registerCommand(
    MODE, 'move', help='move focus in current buffer',
    arguments=[
        (['movement'],
         {'nargs': argparse.REMAINDER,
          'help': '''up, down, [half]page up, [half]page down, first, last, \
                  parent, first reply, last reply, \
                  next sibling, previous sibling, next, previous, \
                  next NOTMUCH_QUERY, previous NOTMUCH_QUERY'''})])
class MoveFocusCommand(MoveCommand):

    def apply(self, ui):
        logging.debug(self.movement)
        tbuffer = ui.current_buffer
        if self.movement == 'parent':
            tbuffer.focus_parent()
        elif self.movement == 'first reply':
            tbuffer.focus_first_reply()
        elif self.movement == 'last reply':
            tbuffer.focus_last_reply()
        elif self.movement == 'next sibling':
            tbuffer.focus_next_sibling()
        elif self.movement == 'previous sibling':
            tbuffer.focus_prev_sibling()
        elif self.movement == 'next':
            tbuffer.focus_next()
        elif self.movement == 'previous':
            tbuffer.focus_prev()
        elif self.movement.startswith('next '):
            query = self.movement[5:].strip()
            tbuffer.focus_next_matching(query)
        elif self.movement.startswith('previous '):
            query = self.movement[9:].strip()
            tbuffer.focus_prev_matching(query)
        elif self.movement.startswith('first '):
            query = self.movement[5:].strip()
            tbuffer.focus_first_matching(query)
        elif self.movement.startswith('last '):
            query = self.movement[9:].strip()
            tbuffer.focus_last_matching(query)
        elif self.movement.startswith('thread'):
            tbuffer.focus_thread_widget()
        elif self.movement.startswith('msg'):
            tbuffer.focus_msg_widget()
        elif self.movement.startswith('toggle'):
            tbuffer.focus_toggle()
        else:
            super().apply(ui)

        # TODO add 'next matching' if threadbuffer stores the original query
        # TODO: add next by date..


RetagPromptCommand = registerCommand(MODE, 'retagprompt')(RetagPromptCommand)


@registerCommand(
    MODE, 'tag', forced={'action': 'add'},
    arguments=[
        (['--all'], {'action': 'store_true',
                     'help': 'tag all messages in thread'}),
        (['tags'], {'help': 'comma separated list of tags'})],
    help='add tags to message(s)',
)
@registerCommand(
    MODE, 'retag', forced={'action': 'set'},
    arguments=[
        (['--all'], {'action': 'store_true',
                     'help': 'tag all messages in thread'}),
        (['tags'], {'help': 'comma separated list of tags'})],
    help='set message(s) tags.',
)
@registerCommand(
    MODE, 'untag', forced={'action': 'remove'},
    arguments=[
        (['--all'], {'action': 'store_true',
                     'help': 'tag all messages in thread'}),
        (['tags'], {'help': 'comma separated list of tags'})],
    help='remove tags from message(s)',
)
@registerCommand(
    MODE, 'toggletags', forced={'action': 'toggle'},
    arguments=[
        (['--all'], {'action': 'store_true',
                     'help': 'tag all messages in thread'}),
        (['tags'], {'help': 'comma separated list of tags'})],
    help='flip presence of tags on message(s)',
)
class TagCommand(Command):

    """manipulate message tags"""
    repeatable = True

    _tags = None

    def __init__(self, tags='', action='add', all=False, **kwargs):
        """
        :param tags: comma separated list of tagstrings to set
        :type tags: str
        :param action: adds tags if 'add', removes them if 'remove', adds tags
                       and removes all other if 'set' or toggle individually if
                       'toggle'
        :type action: str
        :param all: tag all messages in thread
        :type all: bool
        """
        self._tags = frozenset(filter(None, tags.split(',')))
        self.all = all
        self.action = action
        super().__init__(**kwargs)

    async def apply(self, ui):
        if self.all:
            query = 'thread:' + ui.current_buffer.thread.id
        else:
            message = ui.current_buffer.get_selected_message()
            query   = 'id:' + message.id

        if self.action == 'add':
            task = ui.dbman.tags_add(query, self._tags)
        elif self.action == 'set':
            task = ui.dbman.tags_set(query, self._tags)
        elif self.action == 'remove':
            task = ui.dbman.tags_remove(query, self._tags)
        elif self.action == 'toggle':
            write = ui.dbman.db_write_create()

            if self.all:
                messages = ui.current_buffer.thread.messages.values()
            else:
                messages = [ui.current_buffer.get_selected_message()]

            for m in messages:
                to_remove = set()
                to_add    = set()
                for t in self._tags:
                    if t in m.get_tags():
                        to_remove.add(t)
                    else:
                        to_add.add(t)

                write.queue_tag_remove(to_remove, 'id:' + m.id)
                write.queue_tag_add(to_add,       'id:' + m.id)

            task = write.apply()

        try:
            await task
        except DatabaseROError:
            ui.notify('index in read-only mode', priority='error')
            return
