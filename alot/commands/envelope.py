# Copyright (C) 2011-2012  Patrick Totzke <patricktotzke@gmail.com>
# Copyright © 2018 Dylan Baker
# This file is released under the GNU GPL, version 3 or a later revision.
# For further details see the COPYING file

import argparse
import datetime
import email
import glob
import logging
import os
import re
import tempfile
import textwrap
import traceback

from .                      import Command, registerCommand
from .                      import globals
from .                      import utils
from ..                     import buffers
from ..                     import commands
from ..                     import crypto
from ..account              import SendingMailFailed, StoreMailError
from ..db.errors            import DatabaseError
from ..errors               import GPGProblem
from ..settings.const       import settings
from ..settings.errors      import NoMatchingAccount
from ..utils                import argparse as cargparse


MODE = 'envelope'


@registerCommand(
    MODE, 'attach',
    arguments=[(['path'], {'help': 'file(s) to attach (accepts wildcads)'})])
class AttachCommand(Command):
    """attach files to the mail"""
    repeatable = True

    def __init__(self, path, **kwargs):
        """
        :param path: files to attach (globable string)
        :type path: str
        """
        super().__init__(**kwargs)
        self.path = path

    def apply(self, ui):
        envelope = ui.current_buffer.envelope

        files = [g for g in glob.glob(os.path.expanduser(self.path))
                 if os.path.isfile(g)]
        if not files:
            ui.notify('no matches, abort')
            return

        logging.info("attaching: %s", files)
        for path in files:
            envelope.attach_file(path)
        ui.current_buffer.rebuild()


@registerCommand(MODE, 'unattach', arguments=[
    (['hint'], {'nargs': '?', 'help': 'which attached file to remove'}),
])
class UnattachCommand(Command):
    """remove attachments from current envelope"""
    repeatable = True

    def __init__(self, hint=None, **kwargs):
        """
        :param hint: which attached file to remove
        :type hint: str
        """
        super().__init__(**kwargs)
        self.hint = hint

    def apply(self, ui):
        envelope = ui.current_buffer.envelope

        if self.hint is not None:
            for a in envelope.attachments:
                if a.filename and self.hint in a.filename:
                    envelope.attachments.remove(a)
        else:
            envelope.attachments = []
        ui.current_buffer.rebuild()


@registerCommand(MODE, 'refine', arguments=[
    (['key'], {'help': 'header to refine'})])
class RefineCommand(Command):
    """prompt to change the value of a header"""
    def __init__(self, key='', **kwargs):
        """
        :param key: key of the header to change
        :type key: str
        """
        super().__init__(**kwargs)
        self.key = key

    async def apply(self, ui):
        value = ui.current_buffer.envelope.get(self.key, '')
        cmdstring = 'set %s %s' % (self.key, value)
        await ui.apply_command(globals.PromptCommand(cmdstring))


@registerCommand(MODE, 'save')
class SaveCommand(Command):
    """save draft"""
    async def apply(self, ui):
        envelope = ui.current_buffer.envelope

        # determine account to use
        if envelope.account is None:
            try:
                envelope.account = settings.account_matching_address(
                    envelope['From'], return_default=True)
            except NoMatchingAccount:
                ui.notify('no accounts set.', priority='error')
                return
        account = envelope.account

        if account.draft_box is None:
            msg = 'abort: Account for {} has no draft_box'
            ui.notify(msg.format(account.address), priority='error')
            return

        mail = envelope.construct_mail()
        # store mail locally
        path = account.store_draft_mail(mail)

        msg = 'draft saved successfully'

        # add mail to index if maildir path available
        if path is not None:
            ui.notify(msg + ' to %s' % path)
            logging.debug('adding new mail to index')
            try:
                await ui.dbman.msg_add(path, account.draft_tags | envelope.tags)
                await ui.apply_command(commands.globals.BufferCloseCommand())
            except DatabaseError as e:
                logging.error(str(e))
                ui.notify('could not index message:\n%s' % str(e),
                          priority='error',
                          block=True)
        else:
            await ui.apply_command(commands.globals.BufferCloseCommand())


@registerCommand(MODE, 'send')
class SendCommand(Command):
    """send mail"""

    _mail = None

    def __init__(self, mail = None):
        """
        :param mail: email to send
        :type email: email.message.Message
        """
        super().__init__()
        self._mail = mail

    def _get_keys_addresses(self, envelope):
        addresses = set()
        for key in envelope.encrypt_keys.values():
            for uid in key.uids:
                addresses.add(uid.email)
        return addresses

    def _get_recipients_addresses(self, envelope):
        tos = envelope.headers.get('To', [])
        ccs = envelope.headers.get('Cc', [])
        return {a for (_, a) in email.utils.getaddresses(tos + ccs)}

    def _is_encrypted_to_all_recipients(self, envelope):
        recipients_addresses = self._get_recipients_addresses(envelope)
        keys_addresses = self._get_keys_addresses(envelope)
        return recipients_addresses.issubset(keys_addresses)

    async def apply(self, ui):
        envelope        = None
        envelope_buffer = None

        mail = self._mail
        if mail is None:
            # needed to close later
            envelope_buffer = ui.current_buffer
            envelope        = envelope_buffer.envelope

            # This is to warn the user before re-sending
            # an already sent message in case the envelope buffer
            # was not closed because it was the last remaining buffer.
            if envelope.sent_time:
                mod      = envelope.modified_since_sent
                when     = envelope.sent_time
                warning  = 'A modified version of ' * mod
                warning += 'this message has been sent at %s.' % when
                warning += ' Do you want to resend?'
                if (await ui.choice(warning, cancel='no',
                                    msg_position='left')) == 'no':
                    return

            # don't do anything if another SendCommand is in the middle of
            # sending the message and we were triggered accidentally
            if envelope.sending:
                logging.debug('sending this message already!')
                return

            # Before attempting to construct mail, ensure that we're not trying
            # to encrypt a message with a BCC, since any BCC recipients will
            # receive a message that they cannot read!
            if envelope.headers.get('Bcc') and envelope.encrypt:
                warning = textwrap.dedent("""\
                    Any BCC recipients will not be able to decrypt this
                    message. Do you want to send anyway?""").replace('\n', ' ')
                if (await ui.choice(warning, cancel='no',
                                    msg_position='left')) == 'no':
                    return

            # Check if an encrypted message is indeed encrypted to all its
            # recipients.
            if (envelope.encrypt
                    and not self._is_encrypted_to_all_recipients(envelope)):
                warning = textwrap.dedent("""\
                    Message is not encrypted to all recipients. This means that
                    not everyone will be able to decode and read this message.
                    Do you want to send anyway?""").replace('\n', ' ')
                if (await ui.choice(warning, cancel='no',
                                    msg_position='left')) == 'no':
                    return

            clearme = ui.notify('constructing mail (GPG, attachments)…',
                                timeout=-1)

            try:
                mail = envelope.construct_mail()
            except GPGProblem as e:
                ui.clear_notify(clearme)
                ui.notify(str(e), priority='error')
                return

            ui.clear_notify(clearme)

        # determine account to use for sending
        address = mail.get('Resent-From', False) or mail.get('From', '')
        logging.debug("FROM: \"%s\"" % address)
        try:
            account = settings.account_matching_address(address,
                                                        return_default=True)
        except NoMatchingAccount:
            ui.notify('no accounts set', priority='error')
            return
        logging.debug("ACCOUNT: \"%s\"" % account.address)

        # send out
        clearme = ui.notify('sending..', timeout=-1)
        if envelope is not None:
            envelope.sending = True

        try:
            await account.send_mail(mail)
        except SendingMailFailed as e:
            if envelope is not None:
                envelope.account = account
                envelope.sending = False
            ui.clear_notify(clearme)
            logging.error(traceback.format_exc())
            errmsg = 'failed to send: {}'.format(e)
            ui.notify(errmsg, priority='error', block=True)
        except StoreMailError as e:
            ui.clear_notify(clearme)
            logging.error(traceback.format_exc())
            errmsg = 'could not store mail: {}'.format(e)
            ui.notify(errmsg, priority='error', block=True)
        else:
            initial_tags = frozenset()
            if envelope is not None:
                envelope.sending   = False
                envelope.sent_time = datetime.datetime.now()
                initial_tags       = envelope.tags
            logging.debug('mail sent successfully')
            ui.clear_notify(clearme)
            if envelope_buffer is not None:
                cmd = commands.globals.BufferCloseCommand(envelope_buffer)
                await ui.apply_command(cmd)
            ui.notify('mail sent successfully')
            if envelope is not None:
                if envelope.replied:
                    await envelope.replied.tags_add(account.replied_tags)
                if envelope.passed:
                    await envelope.passed.tags_add(account.passed_tags)

            # store mail locally
            # This can raise StoreMailError
            path = account.store_sent_mail(mail)

            # add mail to index if maildir path available
            if path is not None:
                logging.debug('adding new mail to index')
                await ui.dbman.msg_add(path, account.sent_tags | initial_tags)


@registerCommand(MODE, 'edit', arguments=[
    (['--spawn'], {'action': cargparse.BooleanAction, 'default': None,
                   'help': 'spawn editor in new terminal'}),
    (['--refocus'], {'action': cargparse.BooleanAction, 'default': True,
                     'help': 'refocus envelope after editing'})])
class EditCommand(Command):
    """edit mail"""
    def __init__(self, envelope=None, spawn=None, refocus=True, **kwargs):
        """
        :param envelope: email to edit
        :type envelope: :class:`~alot.mail.envelope.Envelope`
        :param spawn: force spawning of editor in a new terminal
        :type spawn: bool
        :param refocus: m
        """
        self.envelope = envelope
        self.openNew = (envelope is not None)
        self.force_spawn = spawn
        self.refocus = refocus
        self.edit_only_body = False
        super().__init__(**kwargs)

    def _headers_to_edit(self, headers):
        whitelist = settings.get('edit_headers_whitelist')
        blacklist = settings.get('edit_headers_blacklist')

        # convert all header names to lowercase for uniqueness
        wl = set(map(str.lower, whitelist))
        bl = set(map(str.lower, blacklist))
        kl = set(map(str.lower, headers))

        # exactly one of wl/bl must be '*', so when operating
        # in blacklist mode we convert the blacklist to whitelist
        if '*' in wl:
            wl = kl - bl

        # drop all non-whitelisted headers
        edit_headers = headers.copy()
        for k in set((k.lower() for k in edit_headers)):
            if not k in wl:
                del edit_headers[k]

        if not '*' in whitelist:
            # any explicitly whitelisted headers that do not have a value
            # are set to empty string, so they appear in the edited file
            for h in whitelist:
                if not h in edit_headers:
                    edit_headers[h] = ''

        logging.debug('editable headers: %s', edit_headers)
        return edit_headers

    async def apply(self, ui):
        ebuffer = ui.current_buffer
        if not self.envelope:
            self.envelope = ui.current_buffer.envelope

        def openEnvelopeFromTmpfile():
            # This parses the input from the tempfile.
            # we do this ourselves here because we want to be able to
            # just type utf-8 encoded stuff into the tempfile and let alot
            # worry about encodings.

            # get input
            # tempfile will be removed on buffer cleanup
            enc = settings.get('editor_writes_encoding')
            with open(self.envelope.tmpfile.name, 'rb') as f:
                template = f.read().decode(enc)

            # call post-edit translate hook
            translate = settings.get_hook('post_edit_translate')
            if translate:
                template = translate(template, ui=ui, dbm=ui.dbman)
            self.envelope.parse_template(template,
                                         only_body=self.edit_only_body)
            if self.openNew:
                ui.buffer_open(buffers.EnvelopeBuffer(self.envelope))
            else:
                ebuffer.envelope = self.envelope
                ebuffer.rebuild()

        # decode header
        headertext = ''
        for key, val in self._headers_to_edit(self.envelope.headers).items():
            # remove to be edited lines from envelope
            if key in self.envelope:
                del self.envelope[key]

            # newlines (with surrounding spaces) by spaces in values
            val = val.strip()
            val = re.sub('[ \t\r\f\v]*\n[ \t\r\f\v]*', ' ', val)
            headertext += '%s: %s\n' % (key, val)

        # determine editable content
        bodytext = self.envelope.body
        if headertext:
            content = '%s\n%s' % (headertext, bodytext)
            self.edit_only_body = False
        else:
            content = bodytext
            self.edit_only_body = True

        # call pre-edit translate hook
        translate = settings.get_hook('pre_edit_translate')
        if translate:
            content = translate(content, ui=ui, dbm=ui.dbman)

        # write stuff to tempfile
        old_tmpfile = None
        if self.envelope.tmpfile:
            old_tmpfile = self.envelope.tmpfile
        with tempfile.NamedTemporaryFile(
                delete=False, prefix='alot.', suffix='.eml') as tmpfile:
            tmpfile.write(content.encode('utf-8'))
            tmpfile.flush()
            self.envelope.tmpfile = tmpfile
        if old_tmpfile:
            os.unlink(old_tmpfile.name)
        cmd = globals.EditCommand(self.envelope.tmpfile.name,
                                  on_success=openEnvelopeFromTmpfile,
                                  spawn=self.force_spawn,
                                  thread=self.force_spawn,
                                  refocus=self.refocus)
        await ui.apply_command(cmd)


@registerCommand(MODE, 'set', arguments=[
    (['--append'], {'action': 'store_true', 'help': 'keep previous values'}),
    (['key'], {'help': 'header to refine'}),
    (['value'], {'nargs': '+', 'help': 'value'})])
class SetCommand(Command):
    """set header value"""
    def __init__(self, key, value, append=False, **kwargs):
        """
        :param key: key of the header to change
        :type key: str
        :param value: new value
        :type value: str
        """
        self.key = key
        self.value = ' '.join(value)
        self.reset = not append
        super().__init__(**kwargs)

    async def apply(self, ui):
        envelope = ui.current_buffer.envelope
        if self.reset:
            if self.key in envelope:
                del envelope[self.key]
        envelope.add(self.key, self.value)
        # FIXME: handle BCC as well
        # Currently we don't handle bcc because it creates a side channel leak,
        # as the key of the person BCC'd will be available to other recievers,
        # defeating the purpose of BCCing them
        if self.key.lower() in ['to', 'from', 'cc'] and envelope.encrypt:
            await utils.update_keys(ui, envelope)
        ui.current_buffer.rebuild()


@registerCommand(MODE, 'unset', arguments=[
    (['key'], {'help': 'header to refine'})])
class UnsetCommand(Command):
    """remove header field"""
    def __init__(self, key, **kwargs):
        """
        :param key: key of the header to remove
        :type key: str
        """
        self.key = key
        super().__init__(**kwargs)

    async def apply(self, ui):
        del ui.current_buffer.envelope[self.key]
        # FIXME: handle BCC as well
        # Currently we don't handle bcc because it creates a side channel leak,
        # as the key of the person BCC'd will be available to other recievers,
        # defeating the purpose of BCCing them
        if self.key.lower() in ['to', 'from', 'cc']:
            await utils.update_keys(ui, ui.current_buffer.envelope)
        ui.current_buffer.rebuild()


@registerCommand(MODE, 'toggleheaders')
class ToggleHeaderCommand(Command):
    """toggle display of all headers"""
    repeatable = True

    def apply(self, ui):
        ui.current_buffer.toggle_all_headers()


@registerCommand(
    MODE, 'sign', forced={'action': 'sign'},
    arguments=[
        (['keyid'],
         {'nargs': argparse.REMAINDER, 'help': 'which key id to use'})],
    help='mark mail to be signed before sending')
@registerCommand(MODE, 'unsign', forced={'action': 'unsign'},
                 help='mark mail not to be signed before sending')
@registerCommand(
    MODE, 'togglesign', forced={'action': 'toggle'}, arguments=[
        (['keyid'],
         {'nargs': argparse.REMAINDER, 'help': 'which key id to use'})],
    help='toggle sign status')
class SignCommand(Command):
    """toggle signing this email"""
    repeatable = True

    def __init__(self, action=None, keyid=None, **kwargs):
        """
        :param action: whether to sign/unsign/toggle
        :type action: str
        :param keyid: which key id to use
        :type keyid: str
        """
        self.action = action
        self.keyid = keyid
        super().__init__(**kwargs)

    def apply(self, ui):
        sign = None
        envelope = ui.current_buffer.envelope
        # sign status
        if self.action == 'sign':
            sign = True
        elif self.action == 'unsign':
            sign = False
        elif self.action == 'toggle':
            sign = not envelope.sign
        envelope.sign = sign

        if sign:
            if self.keyid:
                # try to find key if hint given as parameter
                keyid = str(' '.join(self.keyid))
                try:
                    envelope.sign_key = crypto.get_key(keyid, validate=True,
                                                       sign=True)
                except GPGProblem as e:
                    envelope.sign = False
                    ui.notify(str(e), priority='error')
                    return
            else:
                if envelope.account is None:
                    try:
                        envelope.account = settings.account_matching_address(
                            envelope['From'])
                    except NoMatchingAccount:
                        envelope.sign = False
                        ui.notify('Unable to find a matching account',
                                  priority='error')
                        return
                acc = envelope.account
                if not acc.gpg_key:
                    envelope.sign = False
                    msg = 'Account for {} has no gpg key'
                    ui.notify(msg.format(acc.address), priority='error')
                    return
                envelope.sign_key = acc.gpg_key
        else:
            envelope.sign_key = None

        # reload buffer
        ui.current_buffer.rebuild()


@registerCommand(
    MODE, 'encrypt', forced={'action': 'encrypt'}, arguments=[
        (['--trusted'], {'action': 'store_true',
                         'help': 'only add trusted keys'}),
        (['keyids'], {'nargs': argparse.REMAINDER,
                      'help': 'keyid of the key to encrypt with'})],
    help='request encryption of message before sendout')
@registerCommand(
    MODE, 'unencrypt', forced={'action': 'unencrypt'},
    help='remove request to encrypt message before sending')
@registerCommand(
    MODE, 'toggleencrypt', forced={'action': 'toggleencrypt'},
    arguments=[
        (['--trusted'], {'action': 'store_true',
                         'help': 'only add trusted keys'}),
        (['keyids'], {'nargs': argparse.REMAINDER,
                      'help': 'keyid of the key to encrypt with'})],
    help='toggle if message should be encrypted before sendout')
@registerCommand(
    MODE, 'rmencrypt', forced={'action': 'rmencrypt'},
    arguments=[
        (['keyids'], {'nargs': argparse.REMAINDER,
                      'help': 'keyid of the key to encrypt with'})],
    help='do not encrypt to given recipient key')
class EncryptCommand(Command):
    def __init__(self, action=None, keyids=None, trusted=False, **kwargs):
        """
        :param action: wether to encrypt/unencrypt/toggleencrypt
        :type action: str
        :param keyid: the id of the key to encrypt
        :type keyid: str
        :param trusted: wether to filter keys and only use trusted ones
        :type trusted: bool
        """

        self.encrypt_keys = keyids
        self.action = action
        self.trusted = trusted
        super().__init__(**kwargs)

    async def apply(self, ui):
        envelope = ui.current_buffer.envelope
        if self.action == 'rmencrypt':
            try:
                for keyid in self.encrypt_keys:
                    tmp_key = crypto.get_key(keyid)
                    del envelope.encrypt_keys[tmp_key.fpr]
            except GPGProblem as e:
                ui.notify(str(e), priority='error')
            if not envelope.encrypt_keys:
                envelope.encrypt = False
            ui.current_buffer.rebuild()
            return
        elif self.action == 'encrypt':
            encrypt = True
        elif self.action == 'unencrypt':
            encrypt = False
        elif self.action == 'toggleencrypt':
            encrypt = not envelope.encrypt
        if encrypt:
            if self.encrypt_keys:
                for keyid in self.encrypt_keys:
                    tmp_key = crypto.get_key(keyid)
                    envelope.encrypt_keys[tmp_key.fpr] = tmp_key
            else:
                await utils.update_keys(ui, envelope, signed_only=self.trusted)
        envelope.encrypt = encrypt
        if not envelope.encrypt:
            # This is an extra conditional as it can even happen if encrypt is
            # True.
            envelope.encrypt_keys = {}
        # reload buffer
        ui.current_buffer.rebuild()


@registerCommand(
    MODE, 'tag', forced={'action': 'add'},
    arguments=[(['tags'], {'help': 'comma separated list of tags'})],
    help='add tags to message',
)
@registerCommand(
    MODE, 'retag', forced={'action': 'set'},
    arguments=[(['tags'], {'help': 'comma separated list of tags'})],
    help='set message tags',
)
@registerCommand(
    MODE, 'untag', forced={'action': 'remove'},
    arguments=[(['tags'], {'help': 'comma separated list of tags'})],
    help='remove tags from message',
)
@registerCommand(
    MODE, 'toggletags', forced={'action': 'toggle'},
    arguments=[(['tags'], {'help': 'comma separated list of tags'})],
    help='flip presence of tags on message',
)
class TagCommand(Command):

    """manipulate message tags"""
    repeatable = True

    def __init__(self, tags='', action='add', **kwargs):
        """
        :param tags: comma separated list of tagstrings to set
        :type tags: str
        :param action: adds tags if 'add', removes them if 'remove', adds tags
                       and removes all other if 'set' or toggle individually if
                       'toggle'
        :type action: str
        """
        assert isinstance(tags, str), 'tags should be a unicode string'
        self.tagsstring = tags
        self.action = action
        super().__init__(**kwargs)

    def apply(self, ui):
        ebuffer = ui.current_buffer
        envelope = ebuffer.envelope
        tags = {t for t in self.tagsstring.split(',') if t}
        old = set(envelope.tags)
        if self.action == 'add':
            new = old.union(tags)
        elif self.action == 'remove':
            new = old.difference(tags)
        elif self.action == 'set':
            new = tags
        elif self.action == 'toggle':
            new = old.symmetric_difference(tags)
        envelope.tags = sorted(new)
        # reload buffer
        ui.current_buffer.rebuild()
